import java.util.*;

class Player
{
	public List<Card> cards = new ArrayList<Card>();
	public int id;
	public Player(int player_id) 
	{
		id = player_id;
	}

	public void print_cards()
	{
		System.out.print("Player" + id + ": ");
		Collections.sort(cards);
		for (int i = 0; i < cards.size(); i++)
			System.out.print(cards.get(i).card_string + " ");
		System.out.println();
	}

	public void drop_cards()
	{
		Collections.sort(cards);
		for (int i = 0; i < cards.size() - 1; i++)
		{
			if (cards.get(i).rank_num == cards.get(i + 1).rank_num 
					&& cards.get(i).rank_num != 0)
			{
				cards.remove(i + 1);
				cards.remove(i);
				i--;
			}
		}
	}

	public Card draw_card(Player player)
	{
		System.out.print("Player" + this.id + " draws a card from Player" + player.id + " ");
		Collections.shuffle(player.cards);
		Random rand = new Random();
		int rand_card = rand.nextInt(player.cards.size());
		Card ret = player.cards.get(rand_card);

		this.cards.add(ret);
		player.cards.remove(rand_card);
		return ret;
	}
}
