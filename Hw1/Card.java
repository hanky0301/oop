import java.util.*;

class Card implements Comparable<Card>
{
	public Integer card_num;
	public String card_string;
	public int rank_num;
	public Card(int n)
	{
		card_num = n;
		int suit_num = n % 4;
		rank_num = (n >= 0) ? (n / 4 + 2) : 0;
		String suit_string;
		String rank_string;

		if (rank_num < 11)
			rank_string = Integer.toString(rank_num);
		else if (rank_num == 11)
			rank_string = "J";
		else if (rank_num == 12)
			rank_string = "Q";
		else if (rank_num == 13)
			rank_string = "K";
		else
			rank_string = "A";

		switch (suit_num)
		{
			case -2:
				suit_string = "R";
				break;
			case -1:
				suit_string = "B";
				break;
			case 0:
				suit_string = "C";
				break;
			case 1:
				suit_string = "D";
				break;
			case 2:
				suit_string = "H";
				break;
			case 3:
				suit_string = "S";
				break;
			default:
				suit_string = "";
		}
		card_string = suit_string + rank_string;
	}

	// Compare by card_num
	public int compareTo(Card card) 
	{
		return CardComparator.compare(this, card);
	}

	public static Comparator<Card> CardComparator= new Comparator<Card>()
	{
		public int compare(Card card1, Card card2) 
		{
			return card1.card_num.compareTo(card2.card_num);
		}
	};
}
