import java.util.*;

class Game
{
	static void shuffle_cards(List<Integer> card_nums)
	{
		for (int i = 0; i < 54; i++)
			card_nums.add(i - 2);
		Collections.shuffle(card_nums);
	}

	static void deal_cards(List<Player> players, List<Integer> card_nums)
	{
		System.out.println("Deal cards");
		int curr_deal = 0;
		for (int i = 0; i < 4; i++)
		{
			players.add(new Player(i));
			for (int j = 0; j < (i < 2 ? 14 : 13); j++)
			{
				players.get(i).cards.add(new Card(card_nums.get(curr_deal)));
				curr_deal++;
			}	
			players.get(i).print_cards();
		}	
	}

	static void drop_cards(List<Player> players)
	{
		System.out.println("Drop cards");
		for (int i = 0; i < 4; i++)
		{
			players.get(i).drop_cards();
			players.get(i).print_cards();
		}
	}

	static List<Player> winners(List<Player> players)
	{
		List<Player> winners = new ArrayList<Player>();
		for (int i = 0; i < players.size(); i++)
			if (players.get(i).cards.isEmpty())
				winners.add(players.get(i));
		return winners;
	}

	static void draw_card(List<Player> players, int start)
	{
		for (int draw = start; winners(players).isEmpty(); draw = (draw + 1) % players.size())
		{
			int drew = (draw + 1) % players.size();
			Card card = players.get(draw).draw_card(players.get(drew));
			System.out.println(card.card_string);

			players.get(draw).drop_cards();
			players.get(drew).drop_cards();
			players.get(draw).print_cards();
			players.get(drew).print_cards();
		}
	}

	static int print_winners(List<Player> players)
	{
		List<Player> winners = winners(players);
		if (winners.size() == 1)
		{
			System.out.println("Player" + winners.get(0).id + " wins");
			return players.indexOf(winners.get(0)) % (players.size() - 1);
		}
		else
		{
			System.out.println("Player" + winners.get(0).id + " and " + "Player"
					+ winners.get(1).id + " win");
			return players.indexOf(winners.get(0)) % (players.size() - 2);
		}
	}

	static void play_game(List<Player> players)
	{
		int next_draw;
		System.out.println("Game Start");
		if (winners(players).size() == 0)
			draw_card(players, 0);
		next_draw = print_winners(players);
		System.out.println("Basic game over");

		System.out.println("Continue");
		while (true)
		{
			List<Player> winners = winners(players);
			for (int i = 0; i < winners.size(); i++)
				players.remove(winners.get(i));
			if (players.size() == 1)
				break;
			draw_card(players, next_draw);
			next_draw = print_winners(players);
		}
		System.out.println("Bonus game over");
	}
}
