import java.util.*;

class PlayGame
{
	public static void main(String argv[])
	{
		List<Player> players = new ArrayList<Player>(4);
		List<Integer> card_nums = new ArrayList<Integer>(54);

		// Prepare shuffled card numbers
		Game.shuffle_cards(card_nums);
		
		// Deal cards to each player
		Game.deal_cards(players, card_nums);
		
		// Deal cards to each player
		Game.drop_cards(players);

		// Game start
		Game.play_game(players);
	}
}
