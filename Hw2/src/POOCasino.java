import java.util.*;

class POOCasino
{
	public static void main(String[] argv)
	{
		Player player = Computer.welcome();
	
		/* Open a new deck for the first round */
		Computer.shuffle_cards();
		int bet;
		while ((bet = Computer.collect_bet(player)) != 0)
		{
			Computer.deal_cards(player);
			Computer.replace_cards(player);
			String best_hand = Computer.determine_best_hand(player);
			Computer.payoff(player, best_hand, bet);

			/* Open a new deck for the next round */
			Computer.shuffle_cards();
		}

		Computer.goodbye(player);
		return;
	}
}
