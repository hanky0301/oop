class Test
{
	private int cur_round;
	private int next_card;
	private int card_nums[][] = 
	{
		{44, 48, 40, 32, 36}, 	/* CK CA CQ C10 CJ, royal flush 	*/
		{11, 51, 3, 7, 15},  	/* S4 SA S2 S3 S5,	straight flush 	*/
		{49, 48, 51, 44, 50}, 	/* DA CA SA CK HA,	four_of_a_kind 	*/
		{13, 12, 15, 44, 14}, 	/* D5 C5 S5 CK H5, four_of_a_kind 	*/
		{5, 12, 7, 13, 6}, 		/* D3 C5 S3 D5 H3, full House 		*/
		{13, 4, 15, 7, 14}, 	/* D5 C3 S5 S3 H5, full House 		*/
		{48, 12, 4, 28, 20}, 	/* CA C5 C3 C9 C7, flush 			*/
		{49, 0, 7, 11, 14}, 	/* DA C2 S3 S4 H5, straight 		*/
		{17, 12, 11, 7, 2}, 	/* D6 C5 S4 S3 H2, straight 		*/		
		{9, 48, 11, 10, 14}, 	/* D4 CA S4 H4 H5, three of a kind 	*/
		{9, 48, 23, 51, 50}, 	/* D4 CA S7 SA HA, three of a kind 	*/		
		{9, 20, 27, 23, 22}, 	/* D4 C7 S8 S7 H7, three of a kind 	*/		
		{9, 8, 23, 51, 50}, 	/* D4 C4 S7 SA HA, two pair 		*/
		{9, 44, 47, 51, 50}, 	/* D4 CK SK SA HA, two pair 		*/
		{9, 36, 39, 51, 2}, 	/* D4 CJ SJ SA H2, Jacks or better 	*/
	};
	
	public void set_round(int round)
	{
		cur_round = round;
		next_card = 0;
		return;
	}

	public int get_next()
	{
		return card_nums[cur_round][next_card++];
	}
}
