import java.util.*;

class Player
{
	private String name;
	private int money = 1000;
	private List<Card> cards = new ArrayList<Card>(5);

	public Player(String player_name)
	{
		name = player_name;
		return;
	}

	public String get_name()
	{
		return name;
	}
	public int get_money()
	{
		return money;
	}
	public List<Card> get_cards()
	{
		return cards;
	}

	public void add_dollars(int dollars)
	{
		money += dollars;
		return;
	}
	public void add_card(int n)
	{
		cards.add(new Card(n));
		return;
	}
	public void remove_card(Card card)
	{
		cards.remove(card);
		return;
	}
}
