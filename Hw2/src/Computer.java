import java.util.*;

class Computer
{
	private static int round = 0;
	/* The dealer helps to deal cards to the player */
	//
	private static Shuffler dealer = new Shuffler();
	/* Comment the line above and uncomment the line below for test of currectness */
	//private static Test dealer = new Test();
	//
	/* Set up payoff_map as the payoff table */
	private static Map<String, int[]> payoff_map = new HashMap<String, int[]>(9)
	{
		{
			put("royal flush", new int[] {250, 500, 750, 1000, 4000});
			put("straight flush", new int[] {50, 100, 150, 200, 250});
			put("four of a kind", new int[] {25, 50, 75, 100, 125});
			put("full house", new int[] {9, 18, 27, 36, 45});
			put("flush", new int[] {6, 12, 18, 24, 30});
			put("straight", new int[] {4, 8, 12, 16, 20});
			put("three of a kind", new int[] {3, 6, 9, 12, 15});
			put("two pair", new int[] {2, 4, 6, 8, 10});
			put("Jacks or better", new int[] {1, 2, 3, 4, 5});
		}
	};

	public static Player welcome()
	{
		Scanner scanner = new Scanner(System.in);

		System.out.println("POOCasino Jacks or better, written by b02902075 Hank Chang");
		System.out.print("Please enter your name: ");
		String name = scanner.nextLine();
		/* Initialize the player with his name and 1000 P-dollars */
		Player player = new Player(name);
		System.out.println("Welcome, " + player.get_name() + ".");
		System.out.println("You have " + player.get_money() + " P-dollars now.");

		return player;
	}

	public static void shuffle_cards()
	{
		/* Open a new deck of 52 cards and shuffle it */
		//
		dealer.set_size(52);
		/* Comment the line above and uncomment the line below for test of currectness */
		//dealer.set_round(round);
		//
		return;
	}
	public static int collect_bet(Player player)
	{
		Scanner scanner = new Scanner(System.in);

		System.out.print("Please enter your P-dollar bet for round " + (round + 1)
				+ " (1-5 or 0 for quitting the game): ");
		int bet = scanner.nextInt();
		/* Deduct the amount from the player's money and return the bet */
		player.add_dollars(-bet);
		
		return bet;
	}
	public static void deal_cards(Player player)
	{
		int i;
		char c;

		/* Deal 5 cards to the player */
		for (i = 0; i < 5; i++)
			player.add_card(dealer.get_next());
		
		/* Sort the cards increasingly */
		Collections.sort(player.get_cards());

		System.out.print("Your cards are");
		for (c = 'a', i = 0; c <= 'e'; c++, i++)
			System.out.print(" (" + c + ") " + player.get_cards().get(i).get_card_string());
		System.out.println(".");

		return;
	}
	public static void replace_cards(Player player)
	{
		Scanner scanner = new Scanner(System.in);
		List<Card>cards = player.get_cards();

		System.out.print("Which cards do you want to keep? ");
		/* The "keep" array stores the alphabet indices of the cards the player wants to keep */
		char[] keep = scanner.nextLine().toCharArray();
		/* The "discard" list stores cards that the player wants to remove */
		List<Card> discard = new ArrayList<Card>();

		if (keep.length == 5)
			System.out.println("Okay. I will keep them all.");
		else
		{
			for (int i = 0; i < cards.size(); i++)
			{
				int j;
				for (j = 0; j < keep.length; j++)
					if (i == keep[j] - 'a')
						break;
				/* If the index of the card does not appear in "keep", then add it to "discard" */
				if (j == keep.length)
					discard.add(cards.get(i));
			}
			System.out.print("Okay. I will discard");
			for (int i = 0; i < 5 - keep.length; i++)
				System.out.print(" (" + (char)('a' + cards.indexOf(discard.get(i))) + ") "
									  + discard.get(i).get_card_string());
			System.out.println(".");

			/* Replace each card that appears in "discard" with a new one from the deck */
			for (int i = 0; i < 5 - keep.length; i++)
			{
				player.remove_card(discard.get(i));
				player.add_card(dealer.get_next());
			}
			/* Sort the cards increasingly */
			Collections.sort(cards);

			System.out.print("Your new cards are");
			for (int i = 0; i < 5; i++)
				System.out.print(" " + cards.get(i).get_card_string());
			System.out.println(".");
		}
		
		return;
	}
	public static String determine_best_hand(Player player)
	{
		List<Card> cards = player.get_cards();
		String best_hand;
		boolean flush = false;
		boolean straight = false;
		boolean three_of_a_kind = false;
		int pairs = 0;
		boolean Jacks_or_better = false;
		int i, j;

		/* PREPARATION: Check if there is a flush */
		int suit = cards.get(0).get_suit();
		for (i = 1; i < 5; i++)
			if (suit != cards.get(i).get_suit())
				break;
		if (i == 5)
			flush = true;

		/* PREPARATION: Check if there is a straight */
		int min_rank = cards.get(0).get_rank();
		int max_rank = cards.get(4).get_rank();
		/* If 2 and A are both in the cards, then check for the existence of 3 ~ 5 */
		/* Otherwise, check the next four cards to see if there is a straight */
		int cards_to_check = (min_rank == 2 && max_rank == 14)? 4 : 5;
		for (i = 1; i < cards_to_check; i++)
			if (cards.get(i).get_rank() != min_rank + i)
				break;
		if (i == cards_to_check)
			straight = true;

		/* 1. Check if it is a royal flush */
		if (flush && straight && cards.get(0).get_rank() == 10)
			return "royal flush";

		/* 2. Check if it is a straight flush */
		if (flush && straight)
			return "straight flush";

		/* 3. Check if it is a four of a kind */
		for (i = 0; i < 2; i++)
		{
			for (j = i + 1; j < i + 4; j++)
				if (cards.get(j).get_rank() != cards.get(i).get_rank())
					break;
			if (j == i + 4)
				return "four of a kind";
		}

		/* PREPARATION: Check if there is a three of a kind */
		for (i = 0; i < 3; i++)
		{
			for (j = i + 1; j < i + 3; j++)
				if (cards.get(j).get_rank() != cards.get(i).get_rank())
					break;
			if (j == i + 3)
			{
				three_of_a_kind = true;
				break;
			}
		}

		/* PREPARATION: Count pairs in the cards (one card cannot appear in more than one pair) */
		/* See if there is a Jacks_or_better */
		for (i = 0; i < 4; i++)
		{
			if (cards.get(i + 1).get_rank() == cards.get(i).get_rank())
			{
				for (j = 11; j <= 14; j++)
				{	if (cards.get(i).get_rank() == j)
					{
						Jacks_or_better = true;
						break;
					}
				}
				pairs++;
				i++;
			}
		}

		/* 4. Check if it is a full house */
		if (three_of_a_kind && pairs == 2)
			return "full house";

		/* 5. Check if it is a flush */
		if (flush)
			return "flush";

		/* 6. Check if it is a straight */
		if (straight)
			return "straight";
		
		/* 7. Check if it is a three of a kind */
		if (three_of_a_kind)
			return "three of a kind";
		
		/* 8. Check if it is a two pair */
		if (pairs == 2)
			return "two pair";

		/* 9. Check if it is a Jacks or better */
		if (Jacks_or_better)
			return "Jacks or better";

		/* 10. Others */
		return "";
	}
	public static void payoff(Player player, String best_hand, int bet)
	{
		if (best_hand != "")
		{
			int payoff = payoff_map.get(best_hand)[bet - 1];
			System.out.println("You get a " +  best_hand + ". The payoff is " + payoff + ".");
			player.add_dollars(payoff);
		}
		else
			System.out.println("You get nothing. The payoff is 0.");

		System.out.println("You have " + player.get_money() + " P-dollars now.");
		player.get_cards().clear();
		round++;

		return;
	}

	public static void goodbye(Player player)
	{
		System.out.println("Good bye, " + player.get_name() + ". You played for " + round
										+ (round == 1 ? " round" : " rounds") + " and have " 
										+ player.get_money() + " P-dollars now.");
		return;
	}
}
