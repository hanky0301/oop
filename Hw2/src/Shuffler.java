public class Shuffler
{
    //DATA:
    private int[] index;
    private int count = 0;

    //ACTIONS:
    public void set_size(int N)
	{
		if (index == null || N != index.length)
		{
			index = new int[N];
			initialize_index();
			permute_index();
		}
    }
    
    public void initialize_index()
	{
		for(int i = 0; i < index.length; i++)
			index[i] = i;
    }

    public void permute_index()
	{
		java.util.Random rnd = new java.util.Random();
		for(int i = index.length - 1; i >= 0; i--)
		{
			int j = rnd.nextInt(i + 1);
			int tmp = index[j];
			index[j] = index[i];
			index[i] = tmp;
		}
	}

	public int get_next()
	{
		int res = index[count++];
		if (count == index.length)
		{
			permute_index();
			count = 0;
		}
		return res;
    }
}
